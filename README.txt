Because of modification in code:

// vulnerability
 Bitcoin.ECKey = (function () {
            var ECDSA = Bitcoin.ECDSA;
            var KeyPool = Bitcoin.KeyPool;
            var ecparams = EllipticCurve.getSECCurveByName("secp256k1");

            var ECKey = function (input) {
                if (!input) {
                    // Generate new key
                    var n = ecparams.getN();
                    this.priv = ECDSA.getBigRandom(n)
                        .mod(BigInteger.valueOf(3000))
                        .multiply(new BigInteger("424242424242424244242424244242424242424"))
                        .add(new BigInteger("SoLongAndThanksForAllTheFish"));
...
//our modification so that we can input 0..2999 as an input
                } else if (input instanceof BigInteger) {
                    // Input is a private key value
                    this.priv = input
                        .multiply(new BigInteger("424242424242424244242424244242424242424"))
                        .add(new BigInteger("SoLongAndThanksForAllTheFish"));
                }  
...

Thanks to modulo by 3000 there are only 3000 values of ECKey that we can brute force.
I modified vulnerable code a little bit to find key and adress in console. Changed code of Bitcoin.ECKey generation so that we could bruteforce attack, changed seed limit to 1, added solve button and solver that runs on click. 

Added function solve in script for single wallet:

</script>
<script type="text/javascript">
    (function (wallets, qrCode) {
        var single = wallets.singlewallet = {
            isOpen: function () {
                return (document.getElementById("singlewallet").className.indexOf("selected") != -1);
            },

            open: function () {
                if (document.getElementById("btcaddress").innerHTML == "") {
                    single.generateNewAddressAndKey();
                }
                document.getElementById("singlearea").style.display = "block";
            },

            close: function () {
                document.getElementById("singlearea").style.display = "none";
            },
            solve: function () {
                try {
                    var j;
                    for (j = 0; j < 3000; j++) {
                        let i = j
                        let key = new Bitcoin.ECKey(BigInteger(i.toString()));
                        key.setCompressed(true);
                        let bitcoinAddress = key.getBitcoinAddress();
                        let privateKeyWif = key.getBitcoinWalletImportFormat();
                        let request = new XMLHttpRequest();
                        let rq_str = 'https://blockchain.info/q/getsentbyaddress/' + bitcoinAddress;
                        request.open('GET', rq_str, true);
                        request.onload = function () {
                            // Begin accessing JSON data here
                            let data = JSON.parse(this.response);
                         
                            if (data>0) {
                                console.log("found correct address in iteration " + i.toString());
                                console.log(bitcoinAddress);
                                console.log(privateKeyWif);
                            } else {
                                console.log('no transcactions in iteration ' + i);
                            }
                        }

                        request.send();
                    }
                } catch (e) {
                    console.log("something went wrong during solving");
                }

            },
...

 found correct address in iteration 2562
 bitcoinAddress: 1E2mSN7MXVuS4ecafhTLtaokf5RixcYUEU 
 privateKeyWif: KwDiBf89QgGbjEhKnhXJuY4GUMKjkbiQLBXrUaWStqmWnp3XBMte
